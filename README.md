# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

----

Kokeillaan miten __MarkDown__ menee täällä :

![image.png](./image.png)

Ei mene hyvin Verhojanskissakaan !!

Ainakin kuva menee hyvin mutta näkyykö niin on eri asia.

````
IF (p_kassano IS NOT NULL AND p_kassano > 0) THEN
        SELECT 
            kassano, haltija, DATE_FORMAT(ts,"%d.%m.%Y") AS pvm,
            start, step1, step2, TIMESTAMPDIFF(SECOND,start,NVL(step2,step1)) AS kesto
        FROM yubikey.kelahaku
        WHERE palvelu = 2 AND ts >= p_alkupvm AND kassano = p_kassano
        ORDER BY kesto DESC
        LIMIT 50;
    ELSE
        SELECT
            kassano, haltija, DATE_FORMAT(ts,"%d.%m.%Y") AS pvm,
            start, step1, step2, TIMESTAMPDIFF(SECOND,start,NVL(step2,step1)) AS kesto
            FROM yubikey.kelahaku
            WHERE palvelu = 2 AND ts >= p_alkupvm
            ORDER BY kesto DESC
            LIMIT 50;
    END IF;
````

----

Katsotaan joku muukin kuva vielä `ja esim. tämmöinen quote` 

![image-1.png](./image2.png)
